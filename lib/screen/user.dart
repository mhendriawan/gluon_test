import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controller/user.dart';
import '../widget/alert.dart';
import '../widget/common.dart';

class UserPage extends StatelessWidget {
  const UserPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Padding(
          padding: const EdgeInsets.all(15),
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                const MyLogo(),
                _userText(),
                MyElevatedButton(
                  onPressed: () => UserController.to.handleSubmit(),
                  text: 'Submit',
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _userText() {
    final _txtUser = TextEditingController();

    return Stack(
      alignment: Alignment.centerRight,
      children: [
        Padding(
          padding: const EdgeInsets.all(10),
          child: MyTextFormField(
            controller: _txtUser,
            labelText: 'Username',
            hintText: 'Username',
            prefixIcon: const Icon(Icons.person),
            onChanged: (value) => UserController.to.handleOnChanged(value),
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 20),
          child: GetBuilder<UserController>(
            init: UserController(),
            builder: (_) => AlertInput(
              showPopUp: _.showPopUp,
              isValid: _.isValid,
              handlePopUpCheck: _.handlePopUpCheck,
              handlePopUpError: _.handlePopUpError,
              text: 'Username',
            ),
          ),
        )
      ],
    );
  }
}
