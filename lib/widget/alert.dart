import 'package:flutter/material.dart';

class AlertInput extends StatelessWidget {
  const AlertInput({
    Key? key,
    required this.showPopUp,
    required this.isValid,
    required this.handlePopUpCheck,
    required this.handlePopUpError,
    required this.text,
  }) : super(key: key);

  final bool showPopUp;
  final bool isValid;
  final GestureTapCallback handlePopUpCheck;
  final GestureTapCallback handlePopUpError;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        showPopUp
            ? Container(height: 40)
            : Container(
                height: 40,
                decoration: const BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: Text(
                    '$text cannot be empty',
                    style: const TextStyle(color: Colors.white),
                  ),
                ),
              ),
        const SizedBox(height: 10),
        GestureDetector(
          onTap: isValid ? handlePopUpCheck : handlePopUpError,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 50),
            child: AnimatedSwitcher(
              duration: const Duration(milliseconds: 350),
              transitionBuilder: (child, anim) => RotationTransition(
                turns: child.key == const ValueKey('check')
                    ? Tween<double>(begin: 1, end: 1).animate(anim)
                    : Tween<double>(begin: 1, end: 1).animate(anim),
                child: ScaleTransition(scale: anim, child: child),
              ),
              child: isValid
                  ? const Icon(
                      Icons.check,
                      color: Colors.green,
                      size: 30,
                      key: ValueKey('check'),
                    )
                  : const Icon(
                      Icons.error,
                      color: Colors.red,
                      size: 30,
                      key: ValueKey('error'),
                    ),
            ),
          ),
        )
      ],
    );
  }
}
