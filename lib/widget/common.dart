import 'package:flutter/material.dart';

class MyLogo extends StatelessWidget {
  const MyLogo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(30),
      child: Image.asset('images/logo.jpg', fit: BoxFit.contain),
    );
  }
}

class MyTextFormField extends StatelessWidget {
  const MyTextFormField({
    Key? key,
    required this.controller,
    required this.hintText,
    required this.labelText,
    required this.prefixIcon,
    required this.onChanged,
  }) : super(key: key);

  final TextEditingController controller;
  final String hintText;
  final String labelText;
  final Widget prefixIcon;
  final ValueChanged<String> onChanged;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      onChanged: onChanged,
      decoration: InputDecoration(
        hintText: hintText,
        labelText: labelText,
        prefixIcon: prefixIcon,
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
      ),
    );
  }
}

class MyElevatedButton extends StatelessWidget {
  const MyElevatedButton({
    Key? key,
    required this.onPressed,
    required this.text,
    this.colorButton,
    this.shape,
  }) : super(key: key);

  final VoidCallback onPressed;
  final String text;
  final Color? colorButton;
  final OutlinedBorder? shape;

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(primary: colorButton, shape: shape),
      onPressed: onPressed,
      child: Padding(padding: const EdgeInsets.all(8), child: Text(text)),
    );
  }
}