import 'package:get/get.dart';

class UserController extends GetxController {
  static UserController get to => Get.find();

  bool showPopUp = false;
  bool isValid = false;

  void setBool(value) {
    showPopUp = value;
    isValid = value;
  }

  void handleOnChanged(value) {
    if (value == '') {
      setBool(false);
    } else {
      setBool(true);
    }

    update();
  }

  void handlePopUpError() {
    showPopUp = !showPopUp;

    update();
  }

  void handlePopUpCheck() {}

  void handleSubmit() {
    if (isValid == false) {
      Get.snackbar('Error', "value invalid");
    } else {
      Get.snackbar('Success', "value valid");
    }
  }
}
